# In48hours Backend

Ce repo contient le code source de la backend de In48hours application test pour indatacore.

## Exigence

- [java 8](https://www.oracle.com/java/technologies/java8.html)
- [Srping boot 2.5.0](https://spring.io/projects/spring-boot)
- [Gradle 7.5](https://gradle.org/)


##Deploiment
l'application est deployer a l'aide de azure cloud container instance service

- lien api :http://in48hoursback.westeurope.azurecontainer.io:8080/api/ 
- lien swagger : http://in48hoursback.westeurope.azurecontainer.io:8080/swagger-ui/index.html#/
- lien application client : http://in48hours.westeurope.azurecontainer.io:3000/

## swagger
pour tester les requêtes sur swagger il faut tout dabord générer un accesstoken via JWT
en utilisant l'endpoint /api/authenticate en lui fournissant un username et password
- username: indatadmin
- password: admin123

<a href="https://lh3.googleusercontent.com/QFx_zZBnjiXxJl2hn7N5USMd-_-DqF1MHIviq9ppVhSekq6yyM1W3_prUo87en8z7qtueteH9smvGWlv2VIjC6pVv1KjB44VgwAri7NB_jo0eeitH_urGHJJsXc6ILqZ6eucCjPUNQ=w2400?source=screenshot.guru"> <img src="https://lh3.googleusercontent.com/QFx_zZBnjiXxJl2hn7N5USMd-_-DqF1MHIviq9ppVhSekq6yyM1W3_prUo87en8z7qtueteH9smvGWlv2VIjC6pVv1KjB44VgwAri7NB_jo0eeitH_urGHJJsXc6ILqZ6eucCjPUNQ=w600-h315-p-k"  alt="authenticate endpoint"/> </a>

apres vous receverer le accessToken 

<a href="https://lh3.googleusercontent.com/_FTAb8ZTb8-zNdVdl87-mu0e3-GIUtinDZbHdotXuoprr7GvxS20opM7b29TXIO9SgJiKJTG_uZfvk6QrgM-P8dFMFE80YNgRz8X-ilgTbUGnGos-c2E7oaZF6LUMpnwDM11SgTBdw=w2400?source=screenshot.guru"> <img src="https://lh3.googleusercontent.com/_FTAb8ZTb8-zNdVdl87-mu0e3-GIUtinDZbHdotXuoprr7GvxS20opM7b29TXIO9SgJiKJTG_uZfvk6QrgM-P8dFMFE80YNgRz8X-ilgTbUGnGos-c2E7oaZF6LUMpnwDM11SgTBdw=w600-h315-p-k" alt="receive accessToken"/> </a>

et il faut l'ajouter dans le header comme suite

<a href="https://lh3.googleusercontent.com/YAnRofJH0J4qdFtvKacVHMzsq4wEPmgAVGxUZQi89p4bZQnfqV0eETqAghYN9cMuWTR32wQNE0vNFQh0xqUvgREgn25mszsKfc_omfu4K06A-WheIWN66tuAQjoRQgW8jdMR_5PBDA=w2400?source=screenshot.guru"> <img src="https://lh3.googleusercontent.com/YAnRofJH0J4qdFtvKacVHMzsq4wEPmgAVGxUZQi89p4bZQnfqV0eETqAghYN9cMuWTR32wQNE0vNFQh0xqUvgREgn25mszsKfc_omfu4K06A-WheIWN66tuAQjoRQgW8jdMR_5PBDA=w600-h315-p-k" alt="add accessToken to request header"/> </a>

