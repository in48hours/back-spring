FROM openjdk:8-jdk-alpine

EXPOSE 8080

WORKDIR /app

COPY build/libs/in48hours-final.war .

ENTRYPOINT ["java","-jar","in48hours-final.war"]