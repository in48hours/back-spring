package com.indatacore.in48hours.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Document("Items")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    @Id
    private String id;

    private String name;
    private int quantity;
    private int quantity2;
    private int quantity3;

    public Map<String,Object> toJson(){

        Map<String,Object> map=new HashMap<>();
        map.put("id",this.id);
        map.put("name",this.name);
        map.put("quantity",this.quantity);
        map.put("quantity2",this.quantity2);
        map.put("quantity3",this.quantity3);


        return map;
    }


    public static Item fromList(List<String> line)
    {
        try{
       if(line.size()>2)
           return new Item(null,line.get(0),Integer.parseInt(line.get(1)) ,Integer.parseInt(line.get(2)),Integer.parseInt(line.get(3)));
    else
       if(line.size()>1)
           return new Item(null,line.get(0),Integer.parseInt(line.get(1)) ,Integer.parseInt(line.get(2)),0);

       return new Item();
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            return new Item();
        }
    }

}