package com.indatacore.in48hours.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.HashMap;
import java.util.Map;

@Document("Users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private String id;

    private String username;


    private String password;

    public Map<String,Object> toJson(){

        Map<String,Object> map=new HashMap<>();
        map.put("id",this.id);
        map.put("username",this.username);
        map.put("password",this.password);

        return map;
    }

}
