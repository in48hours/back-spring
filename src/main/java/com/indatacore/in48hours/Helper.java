package com.indatacore.in48hours;

import com.indatacore.in48hours.models.Item;
import com.indatacore.in48hours.repositories.ItemRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

 public abstract class Helper {
   static public String randomString(int length)
   {
       int leftLimit = 97; // letter 'a'
       int rightLimit = 122; // letter 'z'
       int targetStringLength = length>0  ?length : 10;
       Random random = new Random();

      return  random.ints(leftLimit, rightLimit + 1)
               .limit(targetStringLength)
               .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
               .toString();
   }

   static public void readCSV(ItemRepository itemRepo, String path)
   {
       List<List<String>> records = new ArrayList<>();
       try (BufferedReader br = new BufferedReader(
               new FileReader("src/main/resources/items.csv"))) {
           String line;
           int i=0;
           while ((line = br.readLine()) != null) {
               String[] values = line.split(",");
               records.add(Arrays.asList(values));
               if(i>0){
                   System.out.println("creating item : "+records.get(i));
                   itemRepo.save(Item.fromList(records.get(i)));
               }
               i++;
           }
       } catch (IOException e) {
           throw new RuntimeException(e);
       }
   }
}
