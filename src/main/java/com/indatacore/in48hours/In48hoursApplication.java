package com.indatacore.in48hours;

import com.indatacore.in48hours.repositories.ItemRepository;
import com.indatacore.in48hours.repositories.UserRepository;
import com.indatacore.in48hours.security.jwt.JwtUtils;
import com.indatacore.in48hours.services.AdminCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;



@SpringBootApplication
@EnableMongoRepositories
public class In48hoursApplication implements CommandLineRunner {

    @Autowired
    ItemRepository itemRepo;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtUtils jwt;
    @Autowired
    AdminCreator adminC;
    public static void main(String[] args) {

        SpringApplication.run(In48hoursApplication.class, args);
    }

    @Override
    public void run(String... args){
        //CREATE ADMIN
        adminC.createAdmin();
        //READ CSV
        Helper.readCSV(itemRepo,"src/main/resources/items.csv");

    }

}