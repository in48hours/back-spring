package com.indatacore.in48hours.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.indatacore.in48hours.models.Item;
import com.indatacore.in48hours.responses.internalError500Response7;
import com.indatacore.in48hours.services.ItemService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/items")
@CrossOrigin(origins = "*")

public class ItemController {

    @Autowired
    ItemService itemService;

    @PostMapping("")
    public ResponseEntity<Map<String,Object>> AddItem(@RequestBody Item item ) {
        try{
            return itemService.AddItem(item);}
        catch(Exception e)
        {
            return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @ApiOperation(value="Add random item",
            response=Item.class)
    @PostMapping("/random")
    public ResponseEntity<Map<String,Object>> AddItemRandom() {
        try{
            return itemService.AddItemRandom();}
        catch(Exception e)
        {
            return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/bulk")
    public ResponseEntity<Map<String, Object>> DeleteItem(@RequestBody String ids)  {
       try{
        Map<String,Object> result =
                new ObjectMapper().readValue(ids, HashMap.class);
         return itemService.DeleteItems((ArrayList<String>) result.get("ids"));}
        catch(Exception e)
        {
            return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value="Find all items in DB",
            response=Item.class)
    @GetMapping("")
    public ResponseEntity<Map<String,Object>> getItems(){
        try{
            return itemService.getItems();}
        catch(Exception e)
        {
            return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
