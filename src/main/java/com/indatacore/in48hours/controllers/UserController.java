package com.indatacore.in48hours.controllers;

import com.indatacore.in48hours.models.User;

import com.indatacore.in48hours.responses.internalError500Response7;
import com.indatacore.in48hours.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*")

public class UserController {


    @Autowired
    UserService userService;
    @PostMapping("")
    public ResponseEntity<Map<String,Object>> AddUser(@RequestBody User user ) {
try{
       return userService.AddUser(user);}
catch(Exception e)
{
    return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
}
    }


    @GetMapping("")
    public ResponseEntity<Map<String,Object>> getUsers() {
        try{
        return userService.getUsers();
    }catch(Exception e)
        {
            return new ResponseEntity<>(new internalError500Response7(e.getMessage()).send(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}