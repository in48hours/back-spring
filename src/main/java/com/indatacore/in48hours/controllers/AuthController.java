package com.indatacore.in48hours.controllers;

import com.indatacore.in48hours.models.User;
import com.indatacore.in48hours.repositories.UserRepository;
import com.indatacore.in48hours.responses.authentified200Response6;
import com.indatacore.in48hours.responses.invalidPassword400Response4;
import com.indatacore.in48hours.responses.userNotFound400Response3;
import com.indatacore.in48hours.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class AuthController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtUtils jwt;

    @PostMapping("/authenticate")
    public ResponseEntity<Map<String,Object>> authenticate(@RequestBody User user ) {
        Optional<User> response=userRepository.findByUsername(user.getUsername());
        if(response!=null && response.isPresent()) {
            //check password
            if (Objects.equals(response.get().getPassword(), user.getPassword())) {

                String token=jwt.generateTokenFromUsername(user.getUsername());
                Map<String,Object> donne=response.get().toJson();
                donne.put("accessToken",token);

                return new ResponseEntity<>(new authentified200Response6(donne).send(), HttpStatus.OK);

            }
            else
                return new ResponseEntity<>(new invalidPassword400Response4(response.get().toJson()).send(), HttpStatus.BAD_REQUEST);

        }
        else
            return new ResponseEntity<>(new userNotFound400Response3(user).send(), HttpStatus.BAD_REQUEST);
    }
}
