package com.indatacore.in48hours.responses;

import java.util.HashMap;
import java.util.Map;

public class HttpResponse {

    public Map<String, Object> send() {
        Map<String,Object> response=new HashMap<>();
        response.put("status",this.status);
        response.put("code",this.code);
        response.put("message",this.message);
        response.put("donne",this.donne);
return response;
    }

    Object donne;
    int status;
    int code;
    String message;

    public HttpResponse(Object donne,
    int status,
    int code,
    String message)
    {
        this.code=code;
        this.donne=donne;
        this.status=status;
        this.message=message;
    }
}
