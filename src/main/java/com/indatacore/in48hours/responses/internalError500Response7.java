package com.indatacore.in48hours.responses;

public class internalError500Response7 extends HttpResponse {

    public internalError500Response7(String message) {
        super(null,500,7,"something went wrong "+message);
    }

}