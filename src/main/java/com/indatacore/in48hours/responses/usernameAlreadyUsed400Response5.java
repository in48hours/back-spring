package com.indatacore.in48hours.responses;

public class usernameAlreadyUsed400Response5 extends HttpResponse {

    public usernameAlreadyUsed400Response5(String username) {
        super(null,400,5,"user name <"+username+"> already used");
    }

}
