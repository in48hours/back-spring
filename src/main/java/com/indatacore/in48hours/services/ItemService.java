package com.indatacore.in48hours.services;

import com.indatacore.in48hours.Helper;
import com.indatacore.in48hours.models.Item;
import com.indatacore.in48hours.repositories.ItemRepository;
import com.indatacore.in48hours.responses.created201Response1;
import com.indatacore.in48hours.responses.deleted200Response10;
import com.indatacore.in48hours.responses.found200Response2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Component
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public ResponseEntity<Map<String,Object>> AddItem(@RequestBody Item item ) {
        Item response=itemRepository.save(item);
        return new ResponseEntity<>(new created201Response1(response.toJson(),"item").send(), HttpStatus.CREATED);
    }

    public ResponseEntity<Map<String,Object>> AddItemRandom() {

        Item response=itemRepository.save(new Item(null, Helper.randomString(10),
                (int) (Math.random() * 100),
                (int) (Math.random() * 100),(int) (Math.random() * 100)
        ));
        return new ResponseEntity<>(new created201Response1(response.toJson(),"item").send(), HttpStatus.CREATED);
    }

    public ResponseEntity<Map<String,Object>> getItems(){
        List<Item> response=itemRepository.findAll();
        return new ResponseEntity<>(new found200Response2(response.stream()
                .map(Item::toJson),response.size(),"items")
                .send(), HttpStatus.OK);
    }

    public ResponseEntity<Map<String,Object>> DeleteItems(ArrayList<String> ids)
    {

        ids.forEach(i->itemRepository.deleteById(i));
        return new ResponseEntity<>(new deleted200Response10(ids)
                .send(), HttpStatus.OK);
    }

}
