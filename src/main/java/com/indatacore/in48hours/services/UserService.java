package com.indatacore.in48hours.services;

import com.indatacore.in48hours.models.User;
import com.indatacore.in48hours.repositories.UserRepository;
import com.indatacore.in48hours.responses.created201Response1;
import com.indatacore.in48hours.responses.found200Response2;
import com.indatacore.in48hours.responses.usernameAlreadyUsed400Response5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class UserService {
    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Map<String,Object>> AddUser(User user) {

        Optional<User> userExist=userRepository.findByUsername(user.getUsername());

        if(userExist!=null && userExist.isPresent())
            return new ResponseEntity<>(new usernameAlreadyUsed400Response5(user.getUsername()).send(), HttpStatus.BAD_REQUEST);

        User response=userRepository.save(user);
        return new ResponseEntity<>(new created201Response1(response.toJson(),"user").send(), HttpStatus.CREATED);
    }

    public ResponseEntity<Map<String,Object>> getUsers(){
        List<User> response=userRepository.findAll();
        return new ResponseEntity<>(new found200Response2(response.stream()
                .map(User::toJson),response.size(),"users")
                .send(), HttpStatus.OK);
    }
}
