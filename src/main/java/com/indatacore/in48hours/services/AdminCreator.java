package com.indatacore.in48hours.services;

import com.indatacore.in48hours.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

@Component
public class AdminCreator {
    @Autowired
    UserService userService;

    @Value("${in48hours.admin.username}")
    private  String username;
    @Value("${in48hours.admin.password}")


    private String password;
    public void createAdmin(){

        ResponseEntity<Map<String,Object>> response=userService.AddUser(new User(null,username,password));

        if(response!=null && Objects.requireNonNull(response.getBody()).get("code").equals(1))
            System.out.println("admin <"+username+"> created");
        else
            System.err.println("can't create admin <"+username+"> already exist");

    }
}
